<?php

require 'Directions.php';

$routeDecoder = new RouteDecoder();
$points = $routeDecoder->decode('mkk_Ieg_qAiPePsHd[}CzMq@`CaAfCwCvLyApG[xBKZyCpPaDjQ');

echo 'Route has ', sizeof($points), ' points', PHP_EOL;

foreach ($points as $point) {
    echo $point . PHP_EOL;
}
