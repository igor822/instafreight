<?php

require 'TextParser.php';
require 'WordCounter.php';

$stopWords = file_get_contents('stopwords.txt');
$stopWords = array_map('trim', explode("\n", $stopWords));

$textParser = new TextParser('./post1.md');
$textParser->parseContent();
$wordsPost1 = $textParser->getWords();

$textParser = new TextParser('./post2.md');
$textParser->parseContent();
$wordsPost2 = $textParser->getWords();

$counter = new WordCounter($stopWords);
foreach ($wordsPost1 as $line) {
    $counter->addWords($line);
}
foreach ($wordsPost2 as $line) {
    $counter->addWords($line);
}
$counter->addWords($textParser->getTitleWords(), 2);
$wordsCounted = $counter->getCounted();
arsort($wordsCounted);

var_dump(array_slice($wordsCounted, 0, 10));
