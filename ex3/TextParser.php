<?php declare(strict_types=1);

class TextParser
{
    private $file;

    private $title;

    private $words;

    public function __construct(string $file)
    {
        $this->file = $file;
    }

    public function parseContent(): void
    {
        try {
            $file = new SplFileObject($this->file);
        } catch (Exception $e) {
            die($e->getMessage);
        }
        $lineNumber = 0;
        while ($file->valid()) {
            $line = $file->fgets();
            $this->checkLine($line, $lineNumber);
            $lineNumber++;
        }
        $file = null;
    }

    public function checkLine($line, $lineNumber): void
    {
        if (strpos($line, '#') !== false && $lineNumber == 0) {
            $this->title = $line;
        }
        if ($lineNumber > 0 && !in_array($line, ['', PHP_EOL, "\r\n"])) {
            $this->storeWords($line);
        }
    }

    public function getWords(): array
    {
        return $this->words;
    }

    public function getTitleWords(): array
    {
        $words = explode(' ', $this->title);
        return array_map('trim', $words);
    }

    private function storeWords($string): void
    {
        $words = explode(' ', $string);
        $this->words[] = $words;
    }
}
