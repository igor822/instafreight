<?php declare(strict_types=1);

class WordCounter
{
    private $words = [];

    private $stopWords;

    public function __construct(array $stopWords)
    {
        $this->stopWords = $stopWords;
    }

    public function addWords($words, $weight = 1): void
    {
        foreach ($words as $word) {
            if (in_array($word, $this->stopWords)) {
                continue;
            }
            $this->words[] = ['word' => trim($word), 'weight' => $weight];
        }
    }

    public function getCounted(): array
    {
        $array = [];
        foreach ($this->words as ['word' => $word, 'weight' => $weight]) {
            $word = strtolower($word);
            $array[$word] = isset($array[$word]) ? $array[$word] + 1 : 1;
        }
        return $array;
    }
}
