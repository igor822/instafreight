Here are the following codes, all codes should run in PHP 7+.

In case of run the tests in browser, you can use the server built-in of PHP

```bash
php -S localhost:1234
```

And the tests can be accessed by url of `http://localhost:1234/exN/`
