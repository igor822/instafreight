<?php declare(strict_types=1);

require 'Cache.php';

class NewsFeed
{
    private $location;

    private $content;

    private $cache;

    public function __construct(string $location)
    {
        $this->location = $location;
        $this->cache = new Cache(10);
    }

    public function getNews(): array
    {
        if (!$content = $this->cache->get($this->location)) {
            $content = file_get_contents($this->location);
            $this->cache->set($this->location, $content);
        }
        if (!$content) {
            throw new Exception('Fail to retrieve data');
        }
        $news = json_decode($content, true);
        $titles = array_map(
            function($item) {
                echo $item['data']['title'] . PHP_EOL;
            },
            $news['data']['children']
        );

        return array_filter($titles);
    }
}
