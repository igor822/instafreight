<?php declare(strict_types=1);

class Cache
{
    private $location;

    private $ttl = 1;

    public function __construct($ttl = 1)
    {
        $this->location = '/tmp/';
        $this->ttl = $ttl;
    }

    public function exists($key)
    {
        if (file_exists($this->location . md5($key))) {
            return true;
        }
        return false;
    }

    public function set($key, $value): void
    {
        $this->writeContent(md5($key), $value);
    }

    public function get($key)
    {
        $key = md5($key);
        if (!$this->isValid($key)) {
            return false;
        }
        return $this->readContent($key)['data'];
    }

    public function delete($key)
    {
        $this->removeContent($key);
    }

    private function isValid($key)
    {
        $content = $this->readContent($key);
        if (null == $content) {
            return false;
        }
        $dateCreation = new DateTime($content['meta']['timestamp']);
        $dateCreation->add(new DateInterval('PT' . $content['meta']['ttl'] . 'M'));
        if ($dateCreation < new DateTime('now')) {
            $this->delete($key);
            return false;
        }
        return true;
    }

    private function writeContent($filename, $content)
    {
        $content = [
            'meta' => [
                'timestamp' => (new DateTime('now'))->format('Y-m-d H:i:s'),
                'ttl' => $this->ttl
            ],
            'data' => $content
        ];
        $filename = $this->location . $filename;
        $fp = fopen($filename, 'w');
        fwrite($fp, json_encode($content));
        fclose($fp);
    }

    private function readContent($filename)
    {
        $filename = $this->location . $filename;
        if (!file_exists($filename)) {
            return null;
        }
        return json_decode(file_get_contents($filename), true);
    }

    private function removeContent($filename)
    {
        $filename = $this->location . $filename;
        unlink($filename);
    }
}
