<?php

require 'UpliftingNews.php';

try {
    $news = new NewsFeed('https://www.reddit.com/r/UpliftingNews/.json');
    $titles = $news->getNews();
    foreach ($titles as $title) {
        echo $title . PHP_EOL;
    }
} catch (Exception $e) {
    echo $e->getMessage();
}
